import { Injectable } from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {Tour, TourDocument} from "../../schemas/tour";
import {TourDto} from "../../dto/tour-dto";
import {ITourClient} from "../../interfaces/tour";

@Injectable()
export class ToursService {

    toursCount: number = 10;

    constructor(@InjectModel(Tour.name) private tourModel: Model<TourDocument>) {}

    async getTours(): Promise<Tour[]>{
        return this.tourModel.find()
    }

    async getTourById(id: string): Promise<Tour>{
        return this.tourModel.findById(id)
    }

    async removeTours(): Promise<any>{
        return this.tourModel.deleteMany({})
    }

    addTour(data: ITourClient): void{
        const tour = new TourDto(data.name, data.description, data.tourOperator, data.price, data.img);
        const tourData = new this.tourModel(tour);
        tourData.save();
    }

    async getToursByName(name: string): Promise<Tour[]> {
        return this.tourModel.find({name: { "$regex": name, "$options": "i" }})

    }
}
