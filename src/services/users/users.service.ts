import { Injectable } from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {User, UserDocument} from "../../schemas/user";
import {Model} from "mongoose";
import {JwtService} from "@nestjs/jwt";
import {UserDto} from "../../dto/user-dto";

@Injectable()
export class UsersService {

    constructor(@InjectModel(User.name) private userModel: Model<UserDocument>,
                private jwtService: JwtService) {
    }

    async getAllUsers(): Promise<User[]>{
        return this.userModel.find()
    }

    async getUserById(id: string): Promise<User>{
        return this.userModel.findById(id)
    }

    async sendUser(data: UserDto): Promise<User>{
        const userData = new this.userModel(data);
        return userData.save();
    }

    async updateUser(id: string, data): Promise<User>{
        return this.userModel.findByIdAndUpdate(id, data)
    }

    async deleteAllUsers(): Promise<User>{
        return this.userModel.remove()
    }

    async deleteUserById(id: string): Promise<User>{
        return this.userModel.findByIdAndRemove(id)
    }

    async checkAuthUser(login: string, psw: string): Promise<User[]> {
        const users = await this.userModel.find({login: login, psw: psw});
        return users.length === 0 ? null : users;
    }

    async checkRegisterUser(login: string): Promise<User[]> {
        return this.userModel.find({login: login});
    }

    async login(user: UserDto) {
        const payload = {login: user.login, password: user.password};
        const usserFromDb = await this.userModel.find({login: user.login})
        return {
            id: usserFromDb[0]._id,
            access_token: this.jwtService.sign(payload),
        };
    }
}

