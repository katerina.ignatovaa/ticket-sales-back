import { Injectable } from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {Order, OrderDocument} from "../../schemas/order";
import {OrderDto} from "../../dto/order-dto";

@Injectable()
export class OrdersService {

    constructor(@InjectModel(Order.name) private orderModel: Model<OrderDocument>) {}

    sendOrder(data: OrderDto): Promise<Order>{
        const orderData = new this.orderModel(data);
        return orderData.save();
    }

    async getOrders(): Promise<Order[]>{
        return this.orderModel.find()
    }
}
