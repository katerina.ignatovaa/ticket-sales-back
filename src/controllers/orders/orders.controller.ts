import {Body, Controller, Get, Post} from '@nestjs/common';
import {OrdersService} from "../../services/orders/orders.service";
import {OrderDto} from "../../dto/order-dto";
import {Order} from "../../schemas/order";

@Controller('orders')
export class OrdersController {

    constructor(private ordersService: OrdersService) {}

    @Get()
    getOrders(): Promise<Order[]>{
        return this.ordersService.getOrders()
    }

    @Post()
    sendOrder(@Body() data: OrderDto): void{
        this.ordersService.sendOrder(data)
    }

}
++
