import {Body, Controller, Post, UseInterceptors} from '@nestjs/common';
import {FileInterceptor} from "@nestjs/platform-express";
import {diskStorage} from "multer";
import {ToursService} from "../../services/tours/tours.service";
import {ITourClient} from "../../interfaces/tour";

@Controller('tour-item')
export class TourItemController {

    static fileName: string;

    constructor(private toursService: ToursService) {
    }

    @Post()
    @UseInterceptors(FileInterceptor('img', {
        storage: diskStorage({
            destination: './public/',
            filename: (req, file, callback) => {
                const imgType = file.mimetype.split('/');
                const uniqueCode = Date.now();
                const fileName = file.fieldname + '-' + uniqueCode + '.' + imgType[1];
                callback(null, fileName);
                TourItemController.fileName = fileName;
            }
        })
    }))
    addTour(@Body() data): void{
        data.img = TourItemController.fileName;
        this.toursService.addTour(data)
    }
}
