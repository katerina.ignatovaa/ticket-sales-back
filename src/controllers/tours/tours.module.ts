import { Module } from '@nestjs/common';
import { ToursController } from './tours.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {ToursService} from "../../services/tours/tours.service";
import {Tour, TourSchema} from "../../schemas/tour";
import {PassportModule} from "@nestjs/passport";
import {JwtModule} from "@nestjs/jwt";
import {jwtConstants} from "../../static/private/constants";
import {JwtStrategyService} from "../../services/autentification/jwt-strategy/jwt-strategy.service";


@Module({

  imports: [MongooseModule.forFeature([{ name: Tour.name, schema: TourSchema }]),
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
    }),],
  controllers: [ToursController],
  providers: [ToursService, JwtStrategyService],

})
export class ToursModule {}
