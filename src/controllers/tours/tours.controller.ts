import {Body, Controller, Delete, Get, Param, Post, UseGuards, UseInterceptors} from '@nestjs/common';
import {ToursService} from "../../services/tours/tours.service";
import {Tour} from "../../schemas/tour";
import {JwtAuthGuard} from "../../services/autentification/jwt-auth.guard/jwt-auth.guard.service";
import {FileInterceptor} from "@nestjs/platform-express";
import {diskStorage} from "multer";
import {ITourClient} from "../../interfaces/tour";


@Controller('tours')
export class ToursController {

    static fileName: string;

    constructor(private toursService: ToursService) {}

    // @UseGuards(JwtAuthGuard)
    @Get()
    getTours(): Promise<Tour[]>{
        return this.toursService.getTours()
    }

    @Get(':id')
    getTourById(@Param('id') id): Promise<Tour>{
        return this.toursService.getTourById(id)
    }

    @Post()
    @UseInterceptors(FileInterceptor('img', {
        storage: diskStorage({
            destination: './public/',
            filename: (req, file, callback) => {
                const imgType = file.mimetype.split('/');
                const uniqueCode = Date.now();
                const fileName = file.fieldname + '-' + uniqueCode + '.' + imgType[1];
                callback(null, fileName);
                ToursController.fileName = fileName;
            }
        })
    }))
    addTour(@Body() data: ITourClient): void{
        data.img = ToursController.fileName;
        this.toursService.addTour(data)
    }

    @Delete()
    removeTours(): void{
        this.toursService.removeTours()
    }

    @Post(':name')
    getToursByName(@Param('name') name): Promise<Tour[]>{
        return this.toursService.getToursByName(name)
    }

}
