import {Controller, Get, Param, Delete, Post, Body, Put, HttpException, HttpStatus} from '@nestjs/common';
import {UsersService} from "../../services/users/users.service";
import {User} from "../../schemas/user";
import {UserDto} from "../../dto/user-dto";
import {UseGuards} from "@nestjs/common";
import {AuthGuard} from "@nestjs/passport";

@Controller('users')
export class UsersController {

    constructor(private usersService: UsersService) {
    }

    @Get()
    getAllUsers(): Promise<User[]>{
        return this.usersService.getAllUsers()
    }

    @Get(':id')
    getUserById(@Param('id') id): Promise<User>{
        return this.usersService.getUserById(id)
    }

    @Post()
    registerUser(@Body() data: UserDto): Promise<User> {
        return this.usersService.checkRegisterUser(data.login).then((queryResult) => {
            if (queryResult.length === 0) {
                return this.usersService.sendUser(data);
            } else {
                throw new HttpException({
                    status: HttpStatus.CONFLICT,
                    errorText: 'Пользователь уже зарегистрирован'
                }, HttpStatus.CONFLICT);
            }
        });
    }

    @UseGuards(AuthGuard('local'))
    @Post(":login")
    authUser(@Body() data: UserDto, @Param('login') login): any  {
        return this.usersService.login(data);
    }

    @Put(':id')
    updateUser(@Body() data, @Param('id') id): Promise<User>{
        return this.usersService.updateUser(id, data)
    }

    @Delete()
    deleteAllUsers(): Promise<User>{
        return this.usersService.deleteAllUsers()
    }

    @Delete(':id')
    deleteUserById(@Param('id') id): Promise<User>{
        return this.usersService.deleteUserById(id)
    }
}
